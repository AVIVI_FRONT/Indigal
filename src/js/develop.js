function mainSliderInit() {
    var item = $('.js-main-slider');
    if (item.length > 0) {
        item.slick({
            arrows: false,
            // autoplay: true,
            // autoplaySpeed: 2000,
            dots: true
        });
    }
}

function headChecker() {
    $(window).on('scroll', function () {
        var height = $(window).scrollTop();
        var head = $('.header');
        var content = $('.main');
        var button = $('.js-buy-btn');
        if (button.hasClass('active')) {
            button.removeClass('active');
            $('.header__triangle').addClass('active');
            $('.header__cross').removeClass('active');
            $('.buy').removeClass('active');
        }
        if (height > 105) {
            head.addClass('fixer');
            content.addClass('fixer');

        } else {
            head.removeClass('fixer');
            content.removeClass('fixer');
        }
        var wrapper = document.getElementsByClassName('global-wrapper')[0];
        var low_box = document.getElementsByClassName('last__wrap')[0];
        var button = document.getElementsByClassName('header__call')[0];
        console.log(parseInt($(window).scrollTop()) + parseInt(window.innerHeight));
        if (parseInt($(window).scrollTop()) + parseInt(window.innerHeight) > (wrapper.scrollHeight - low_box.scrollHeight))  {
            button.style.bottom = low_box.scrollHeight + button.scrollHeight + 'px';
        } else {
            button.style.bottom = '15px';
        }
    })
}

function mapDrag() {
    var pic = document.getElementById('picture');
    pic.addEventListener("touchmove", dragg, false);
    function dragg() {
        var obj = $('.place');
        var mas = [];
        mas.push(obj);
        for (var k = 0; k < mas[0].length; k++) {
            var element = mas[0][k];
            if (element.getBoundingClientRect().left <  parseInt(pic.getBoundingClientRect().left)  + parseInt(pic.offsetWidth/2)) {
                $(element).addClass('active');
            } else {
                $(element).removeClass('active');
            }
            if (element.getBoundingClientRect().left <  parseInt(pic.getBoundingClientRect().left) - parseInt(element.offsetWidth/4) ) {
                $(element).removeClass('active');
            }
        }
        return false;
    }
}

function scrollMap() {
    var pic = document.getElementById('picture');
    pic.addEventListener("mousewheel", MouseWheelHandler, false);
    function MouseWheelHandler(e)
    {
        var e = window.event || e; // old IE support
        e.preventDefault();
        var delta = Math.max(-1, Math.min(1, (e.wheelDelta || -e.detail)));
        pic.scrollLeft -= (delta * 40);
        var obj = $('.place');
        var mas = [];
        mas.push(obj);
        for (var k = 0; k < mas[0].length; k++) {
            var element = mas[0][k];
            if (element.getBoundingClientRect().left <  parseInt(pic.getBoundingClientRect().left)  + parseInt(pic.offsetWidth/2)) {
                $(element).addClass('active');
            } else {
                $(element).removeClass('active');
            }

            if (element.getBoundingClientRect().left <  parseInt(pic.getBoundingClientRect().left) - parseInt(element.offsetWidth/4) ) {
                $(element).removeClass('active');
            }
        }
        return false;
    }
}

function articleOpen() {
    $(document).on('click', '.happens__box', function (e) {
        e.stopPropagation();
        var button = $(this);
        var field = $(this).parents('.happens').find('.happens__article');
        e.preventDefault();
        if (button.hasClass('active')) {
            button.removeClass('active');
            field.removeClass('active');
        } else {
            $('.happens__box').removeClass('active');
            $('.happens__article').removeClass('active');
            $('.happens').removeClass('active');
            button.addClass('active');
            field.addClass('active');
        }
    });
    $(document).on('click', '.js-art-btn', function (e) {
        e.stopPropagation();
        $(this).parents('.happens__article').removeClass('active');
        $('.happens__box').removeClass('active');
    });

    $(document).on('click touchstart',function (event){
        var button = $('.happens__box');
        var field = button.parents('.happens').find('.happens__article');
        if (!field.is(event.target) && field.has(event.target).length === 0 && !button.is(event.target) && button.has(event.target).length === 0) {
            button.removeClass('active');
            field.removeClass('active');
        }
    });

}

function startTest() {
    $(document).on('click', '.js-start-test', function () {
        $('.quest__form').addClass('active');
        $('.quest__question:first-child').addClass('active');
        $(this).css('display', 'none');
    })
}
function changeQuestion() {
    var item = $('.quest__question');
    console.log(item.length);
    $(document).on('change', '.js-answer input', function () {
        var index = $(this).parents('.quest__question').index();
        $('.js-test-count').text(index + 1);
        var all = item.length;
        var wid = (index+1) * (100/all) + '%';
        if (index < item.length - 1) {
            item.eq(index).addClass('hidden');
            setTimeout(function () {
                item.eq(index).removeClass('hidden');
            }, 300);
            item.eq(index).removeClass('active');
            item.eq(index + 1).addClass('active');
            console.log($(this));
            wid = (index+1) * (100/all) + '%';
            $('.test__complete').css('width', wid);
        } else {
            wid = (index+1) * (100/all) + '%';
            $('.test__complete').css('width', wid);
            console.log($('.quest__form').serialize());
            // $.ajax({
            //     url: "request.php",
            //     type: "POST",
            //     data: $('.quest__form').serialize(),
            //     success: function(result){
            //         $(".advice").html(result);
            // }});

        }
    })
}


function showMenu() {
    $(document).on('click', '.js-menu', function () {
        var btn = $(this);
        var list = $('.header__list');
        var menu = $('.header__mid');
        var button = $('.js-buy-btn');
        var cross = $('.header__cross');
        var tri = $('.header__triangle');
        var buy = $('.buy');
        if (btn.hasClass('active')) {
            btn.removeClass('active');
            list.removeClass('active');
            menu.removeClass('active');
        } else {
            $('body').removeClass('hider');
            btn.addClass('active');
            list.addClass('active');
            menu.addClass('active');
            if (button.hasClass('active')) {
                button.removeClass('active');
                cross.removeClass('active');
                tri.addClass('active');
                buy.removeClass('active');
            }
        }
    })
}

function checkChange(item) {
    var brn = $('.js-send-mess');
    if (item.checked) {
        brn.removeClass('dsb');
    } else {
        brn.addClass('dsb');
    }
}

function showMagazines() {
    $(document).on('click','.js-buy-btn', function (e) {
        e.preventDefault();
        var button = $(this);
        var cross = $('.header__cross');
        var tri = $('.header__triangle');
        var buy = $('.buy');

        var btn = $('.js-menu');
        var list = $('.header__list');
        var menu = $('.header__mid');
        if (button.hasClass('active')) {
            button.removeClass('active');
            cross.removeClass('active');
            tri.addClass('active');
            buy.removeClass('active');
            $('body').removeClass('hider');
        } else {
            $('body').addClass('hider');
            button.addClass('active');
            cross.addClass('active');
            tri.removeClass('active');
            buy.addClass('active');
            if (btn.hasClass('active')) {
                btn.removeClass('active');
                list.removeClass('active');
                menu.removeClass('active');
            }
        }
    })
}

function smoothScroll() {
    $(document).on("click","a.js-scroll", function (event) {
        var height = $('.header__main').height();
        event.preventDefault();
        event.stopPropagation();
        var id  = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({scrollTop: (top - height)}, 500);
    });
}

function openType() {
    $(document).on('click', '.type', function (e) {
        e.preventDefault();
        var btn = $(this);
        var field = btn.find('.open');
        if (btn.hasClass('active')) {
            btn.removeClass('active');
            field.removeClass('active');
        } else {
            btn.addClass('active');
            field.addClass('active');
        }
    });
    $(document).on('click','.js-open-btn', function (e) {
        e.stopPropagation();
        e.preventDefault();
        console.log('clicked');
        $('.type').removeClass('active');
        $('.open').removeClass('active');

    });
    $(document).on('click touchstart',function (event){
        var button = $('.type');
        var field = button.find('.open');
        if (!field.is(event.target) && field.has(event.target).length === 0 && !button.is(event.target) && button.has(event.target).length === 0) {
            button.removeClass('active');
            field.removeClass('active');
        }
    });
}

$(document).ready(function () {
    mainSliderInit();
    headChecker();
    scrollMap();
    articleOpen();
    startTest();
    changeQuestion();
    showMenu();
    showMagazines();
    smoothScroll();
    openType();
    mapDrag();
});